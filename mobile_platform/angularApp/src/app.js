angular.module('angularApp',['ngPasswordStrength', , 'ui.mask', 'ui.bootstrap', 'ui.bootstrap.contextMenu', 'ui.router',"xeditable" , "ngTable", "infinite-scroll", 'ngResource', 'openfb', "angularApp.controllers", "angularApp.services"])
.factory('authInterceptor', function ($rootScope, $q, $window) {
      return {
        request: function (config) {
          config.headers = config.headers || {};
          if ($window.sessionStorage.token) {
            config.headers.Authorization = 'Token ' + $window.sessionStorage.token;
          }
          return config;
        },
        responseError: function (rejection) {
          if (rejection.status === 401) {
            // handle the case where the user is not authenticated
            delete $window.sessionStorage.token;
          }
          return $q.reject(rejection);
        }
      };
})
.config(function($stateProvider, $urlRouterProvider, socialProvider, $httpProvider){

      socialProvider.setFbKey({appId: '1030952180306942', apiVersion: 'v2.7'});

      //$urlRouterProvider.otherwise("/home");
      $stateProvider
        .state('home', {
            url: "/",
            templateUrl: "partials/home.html",
            controller: 'HomeCtrl'
        });
      $stateProvider
        .state('companies_list', {
            url: "/",
            templateUrl: "partials/companies_list.html",
            controller: 'CompaniesListCtrl'
        });
      $stateProvider
      .state('company', {
            url: '/company/:id',
            templateUrl: "partials/company.html",
            controller: 'CompanyCtrl'
        });
      $stateProvider
      .state('my_rewards', {
            url: '/my_rewards',
            templateUrl: "partials/my_rewards.html",
            controller: 'MyRewardsCtrl'
        });
      $stateProvider
      .state('account', {
            url: '/accounts',
            templateUrl: "partials/account.html",
            controller: 'AccountCtrl'
        });
      $stateProvider
      .state('status', {
            url: '/status',
            templateUrl: "partials/status.html"
      });
      $stateProvider
      .state('password', {
            url: '/password',
            templateUrl: "partials/password.html",
            controller: 'PasswordCtrl'
      });
      $stateProvider
      .state('social', {
            url: '/social',
            templateUrl: "partials/social.html"
      });
      $stateProvider
      .state('qr', {
            url: '/qr/:code',
            templateUrl: "partials/qr.html",
            controller: 'QrCodeCtrl'
      });
      $stateProvider
      .state('register', {
            url: "/register",
            templateUrl: "partials/register.html",
            controller: 'RegisterCtrl'
        })
      $stateProvider
      .state('login', {
            url: "/login",
            templateUrl: "partials/login.html"
      })
      .state('activation', {
            url: "/activation",
            templateUrl: "partials/activation.html"
      });
      $httpProvider.interceptors.push('authInterceptor');
 })
.run(function(editableOptions, OpenFB) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
    OpenFB.init('1030952180306942');
}).run(function(RewardsNotificationCenter){});
