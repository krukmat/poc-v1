angular.module('angularApp.controllers', ['angularApp.services', 'ngToast', 'ja.qr', 'ui.bootstrap.datetimepicker'])
.controller('HomeCtrl', function($window, $http, $scope, $resource, CompanyService, CompanySearch, AuthService, $location) {
   $scope.indexClass = 'home';
   AuthService.me()
        .then(function (data) {
            $scope.profile = data.data;
            $scope.isAuthenticated = true;
        }, function (data) {
            $scope.isAuthenticated = false;
            $location.path("/login");
        });
})
.controller('MyRewardsCtrl', function($window, $http, $scope, $resource, CompanyService, CompanySearch, AuthService, $location) {
   $scope.indexClass = 'home';
   AuthService.me()
        .then(function (data) {
            $scope.profile = data.data;
            $scope.isAuthenticated = true;
        }, function (data) {
            $scope.isAuthenticated = false;
            $location.path("/login");
        });
})
.controller('CompaniesListCtrl', function($window, $http, $scope, $resource, CompanyService, CompanySearch, AuthService, CategoryService, $location) {
    // TODO: Borrar
})
.controller('CompanyCtrl', function($window, $http, $scope, $resource,  $stateParams, CompanyService, AuthService, SubscriptionService, PointsService, CompanyUpdateService, $location, newRewardService, ngToast, modalService) {
  $scope.indexClass = 'home';

   var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'partials/modal.html'
        };

     var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Proceed?',
            bodyText: 'Perform this action?'
     };

  //TODO: Refactor this. Move it into service. Handle only the data here
  function search(pk, companies){
    for (var i=0; i < companies.length; i++) {
        if (companies[i].company.pk === pk) {
            return companies[i];
        }
    }
    return null;
   }
  AuthService.me()
        .then(function (data) {
            $scope.profile = data.data;
            $scope.isAuthenticated = true;
            $scope.is_subscribed = search(parseInt($stateParams.id), $scope.profile.subscriptions);

            var promise = CompanyService.get({id: $stateParams.id}).$promise.then(function(_response) {
                    $scope.object = _response;
            });
        }, function (data) {
            $scope.isAuthenticated = false;
            $location.path("/login");
        });
  $scope.edit_share_company_points = function(company){
    console.log(company);
    modalService.showModal(modalDefaults, modalOptions).then(function (result) {
            console.log(result.modalOptions.points);
            company.share_company_points = parseInt(result.modalOptions.points);
            // sumar puntos al usuario
            CompanyUpdateService.update(company).then(
                function (data) {
                    console.log(data);
                }, function (data) {
                    console.log(data);
                }
            );
     });
  }
  $scope.edit_share_reward_points = function(company) {
    modalService.showModal(modalDefaults, modalOptions).then(function (result) {
            console.log(result.modalOptions.points);
            company.share_reward_points = parseInt(result.modalOptions.points);
            // sumar puntos al usuario
            CompanyUpdateService.update(company).then(
                function (data) {
                    console.log(data);
                }, function (data) {
                    console.log(data);
                }
            );
     });
  }
  // Add reward
  $scope.add_reward = function(company) {
        var reward = {type:'PHYSICAL'};
        newRewardService.showModal(null, null, reward).then(function (result) {
            result.modalOptions.reward.company = company;
            AuthService.add_reward(result.modalOptions.reward).then(
                function (data) {
                    CompanyService.get({id: company}).$promise.then(function(_response) {
                        $scope.object = _response;
                    });
                }, function (data) {
                    console.log(data);
                }
            );
        });
  }
  $scope.edit_reward = function(company, reward) {
        console.log(reward);
        newRewardService.showModal(null, null, reward).then(function (result) {
            AuthService.update_reward(result.modalOptions.reward).then(
                function (data) {
                    CompanyService.get({id: company}).$promise.then(function(_response) {
                        $scope.object = _response;
                    });
                }, function (data) {
                    console.log(data);
                }
            );
        });
  }
  $scope.delete_reward = function(company, reward) {
    let modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'partials/delete-modal.html'
    };
    modalService.showModal(modalDefaults, modalOptions).then(function (result) {
            AuthService.delete_reward(reward).then(
                function (data) {
                    CompanyService.get({id: company}).$promise.then(function(_response) {
                        $scope.object = _response;
                    });
                }, function (data) {
                    console.log(data);
                }
            );
     });
  }
})
.controller('RegisterCtrl', function($scope, $http, $window, $location, AuthService){
      $scope.isRegister = true;
      $scope.isAuthenticated = false;
      $scope.register = function () {
                AuthService.register($scope.user)
                .then(function (data) {
                  $scope.error = 'Chequee su casilla de correo para activar la cuenta';
                }, function (data) {
                  // Erase the token if the user fails to log in
                  delete $window.sessionStorage.token;
                  $scope.isAuthenticated = false;

                  // Handle login errors here
                  $scope.error = 'Error: Problem in register';
                  $scope.welcome = '';
                });
            };
})
.controller('UserCtrl', function ($rootScope, $scope, $http, $window, $location, AuthService) {
      $scope.user = {email: '', password: ''};
      $scope.isAuthenticated = false;
      $scope.isRegister = false;
      $scope.welcome = '';
      $scope.message = '';

      $rootScope.$on('event:social-sign-in-success', function(event, userDetails){
          console.log(userDetails);
      });

      $scope.selected = 'None';
      $scope.menuOptions = [
          ['Mi cuenta', function ($itemScope) {
              $location.path("/accounts");
          }],
          ['Actualizar Contraseña', function ($itemScope) {
              $location.path("/password");
          }],
          ['Salir', function ($itemScope) {
              $scope.logout();
              $scope.indexClass = '';
              $location.path('/login');
          }]
      ];

      $scope.has_social = function(social) {
          var social_exists = false;
          if ($scope.profile) {
            console.log($scope.profile.social_accounts);
            $scope.profile.social_accounts.forEach(function(social_network) {
                  if (social_network.social === social) {
                      social_exists = true;
                  }
            });
            return social_exists;
          }
          return true;
      }

      $scope.login = function () {
          AuthService.login($scope.user)
          .then(function (data) {
            $window.sessionStorage.token = data.data.auth_token;
            $scope.isAuthenticated = true;
            $scope.indexClass = 'home';
            $location.path("/");
            location.reload();
          }, function (data) {
            // Erase the token if the user fails to log in
            // delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;
            $scope.indexClass = '';

            // Handle login errors here
            $scope.error = data.data.non_field_errors[0];
            $scope.welcome = '';
          });
      };

      $scope.register = function () {
          AuthService.register($scope.user)
          .then(function (data, status, headers, config) {
            $location.path("/login");
          }, function (data) {
            // Erase the token if the user fails to log in
            delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;

            // Handle login errors here
            $scope.error = 'Error: Invalid user or password';
            $scope.welcome = '';
          });
      };

      $scope.logout = function () {
        $scope.welcome = '';
        $scope.message = '';
        $scope.isAuthenticated = false;
        delete $window.sessionStorage.token;
      };

      $scope.callRestricted = function () {
        AuthService.me()
        .then(function (data) {
          $scope.isAuthenticated = true;
          $scope.profile = data.data;
          $scope.indexClass = 'home';
        }, function (data) {
          $scope.isAuthenticated = false;
          $scope.indexClass = '';
          $location.path("/login");
        });
      };
      $scope.callRestricted()

    })
.controller('AccountCtrl', function($scope, AuthService, ngToast) {
  $scope.update = function update(){
      AuthService.update_me($scope.profile);
      ngToast.create({
        className: 'success',
        content: 'Updated',
        dismissButton: true
      });
  }
})
.controller('PasswordCtrl', function($scope, AuthService, ngToast) {
  $scope.passwords = {};
  $scope.passwords.new_password = '';

  $scope.update = function update(){
      AuthService.update_password($scope.passwords);
      ngToast.create({
        className: 'success',
        content: 'Password Updated',
        dismissButton: true
      });
  }
})
.controller('RewardsExchangeCtrl', function($stateParams, $scope, AuthService, $location) {
    AuthService.rewards_to_exchange().then(
        function (data) {
              $scope.reward_exchange = data.data.results;
        }, function (data) {
    });
    $scope.canjear = function(item) {
        AuthService.update_reward_exchange(item).then(
            function(data){
                console.log(data);
                AuthService.rewards_to_exchange().then(
                    function (data) {
                          $scope.reward_exchange = data.data.results;
                    }, function (data) {
                    }
                );
            }
        ,);
    }
})
.controller('SubscribersFinderCtrl', function($stateParams, $scope, AuthService, $location, modalService) {

     var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'partials/modal.html'
        };

     var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Proceed?',
            bodyText: 'Perform this action?'
     };

    AuthService.subscribers_list().then(
        function (data) {
              $scope.subscribers_list = data.data.results;
        }, function (data) {
        }
    );
    $scope.add_points = function(item) {
        modalService.showModal(modalDefaults, modalOptions).then(function (result) {
            console.log(result.modalOptions.points);
            item.points_pending+=parseInt(result.modalOptions.points);
            // sumar puntos al usuario
            AuthService.update_subscription(item).then(
                function (data) {
                    console.log(data);
                }, function (data) {
                    console.log(data);
                }
            );
        });
    }
})
.controller('QrCodeCtrl', function($stateParams, $scope, AuthService, $location) {
    // TODO: Borrar
});
