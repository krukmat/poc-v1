from django import forms
from django.contrib import admin
from django.db.models.fields.related import ManyToManyRel
from django.contrib.admin.widgets import RelatedFieldWidgetWrapper

from models import *

class SubscriptionForm(forms.ModelForm):
    user = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))

    def __init__(self, *args, **kwargs):
        super(SubscriptionForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.initial['user'] = self.instance.mypointsuser_set.all()[0].email


class MyPointsUserForm(forms.ModelForm):
    subscriptions = forms.ModelMultipleChoiceField(
        Subscription.objects.all(),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(MyPointsUserForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            # if this is not a new object, we load related books
            self.fields['subscriptions'].queryset = self.instance.subscriptions
            #self.initial['subscriptions'] = self.instance.subscriptions.values_list('pk', flat=True)

    def save(self, *args, **kwargs):
        instance = super(MyPointsUserForm, self).save(*args, **kwargs)
        if instance.pk:
            for book in instance.subscriptions.all():
                if book not in self.cleaned_data.get('subscriptions',[]):
                    # we remove books which have been unselected
                    instance.subscriptions.remove(book)
            for sub in self.cleaned_data.get('subscriptions', []):
                if sub not in instance.subscriptions.all():
                    # we add newly selected books
                    instance.subscriptions.add(sub)
        return instance