# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0004_socialaccount'),
    ]

    operations = [
        migrations.AddField(
            model_name='socialaccount',
            name='social',
            field=models.CharField(default=b'', max_length=255, null=True),
        ),
    ]
